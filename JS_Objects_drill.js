const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

// Complete the following underscore functions.
// Reference http://underscorejs.org/ for examples.

function keys(obj) {
  // Retrieve all the names of the object's properties.
  // Return the keys as strings in an array.
  // Based on http://underscorejs.org/#keys
  var arr=[]
  for (elements in obj){
    arr.push(elements)
  }
  return arr
}
console.log(keys(testObject));


function values(obj) {
  // Return all of the values of the object's own properties.
  // Ignore functions
  // http://underscorejs.org/#values
  arr=[];
    for(let i in obj){
        arr.push(obj[i])
    }
    return arr;
}
console.log(value(testObject));



function mapObject(obj, cb) {
  // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
  // http://underscorejs.org/#mapObject
  for(let element in obj){
    obj[element]=cb(element)
    }
    return obj
  }
  cb = (value)=> value + 5  // Here, I transforming the value by adding 5 in it
  console.log(mapObject(testObject, cb));



function pairs(obj) {
  // Convert an object into a list of [key, value] pairs.
  // http://underscorejs.org/#pairs
  arr=[];
  for(element in obj){
      arr.push([element,obj[element]])
  }
  return arr
}
console.log(pairs(testObject));

/* STRETCH PROBLEMS */

function invert(obj) {
  // Returns a copy of the object where the keys have become the values and the values the keys.
  // Assume that all of the object's values will be unique and string serializable.
  // http://underscorejs.org/#invert
  revArray={};
     for(element in obj){
       let valve=obj[element];
       revArray[valve]=element;
     }
     return revArray;
}
console.log(invert(testObject));

function defaults(obj, defaultProps) {
  // Fill in undefined properties that match properties on the `defaultProps` parameter object.
  // Return `obj`.
  // http://underscorejs.org/#defaults

  let obj2=defaultProps;
  for(let element in obj2){
    if(element in obj){
      obj2[element]=obj[element]
    }
    obj[element]=obj2[element]
  }
  return obj
}
console.log(defaults(testObject,{name:'Tony Stark', Company:'Stark Interprises'}));
 