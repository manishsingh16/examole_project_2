function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    var counter=value
    var increment=function(counter){
            counter=counter+1;
            console.log(counter);
    }

    var decrement=function(counter){
            counter=counter-1;
            console.log(counter);
    }
    return {increment: increment,
            decrement: decrement}; 
}

var fr = conterFactory(0);
fr.decrement(4);
fr.increment(5);




  function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    let counter =1
    return () => {
      if(counter <= n){
      cb()
      counter += 1
    }
    else
    return null
   }
   }
   const cb= () => {
     console.log('function call')
   } 
   
   const funCall = limitFunctionCallCount(cb, 3) 
   funCall()
   funCall()
   funCall()
   funCall()
   funCall()
   funCall()

   


  
  
  function cacheFunction(cb) {
    // Should return a funciton that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    let cache ={}
    return(...args) =>{
      const key =JSON.stringify([...args])
      if(key in cache){
        console.log('From Cache')
        return cache[key]
      } else{
        const result = cb(...args)
        cache[key] = result
        console.log('From calculating')
        return result
      }
    }
    }
      const add= (n,m,t) => t+n+m+10
      const cacheadd =cacheFunction(add)
      console.log(cacheadd(2,4,6))
      console.log(cacheadd(2,4,6))
      console.log(cacheadd(2,4,6))
      console.log(cacheadd(3,5,7))
      console.log(cacheadd(3,5,7))
      console.log(cacheadd(5,9,16))
    
  